INSERT INTO [RIS].[dbo].[tRoles]
           ([Name],[Description],[CreatedBy],[CreationDate],[LastUpdatedBy],[LastUpdateDate])
     VALUES('Admin','RIS Administrator',1,1/1/1,1,1/1/1);

INSERT INTO [RIS].[dbo].[tRoles]
           ([Name],[Description],[CreatedBy],[CreationDate],[LastUpdatedBy],[LastUpdateDate])
     VALUES('Radiologist','Radiologist',1,1/1/1,1,1/1/1);
 
INSERT INTO [RIS].[dbo].[tRoles]
           ([Name],[Description],[CreatedBy],[CreationDate],[LastUpdatedBy],[LastUpdateDate])
     VALUES('Referring Physician','Referring Physician',1,1/1/1,1,1/1/1);
