// Generated by CodeGenerator
// DO NOT MODIFY!
using RIS.RISLibrary.Fields;
using System;
namespace RIS.RISLibrary.Objects.RIS
{
	public class StationObject : RISObject
	{
		override public string GetTableName()
		{
			return "tStations";
		}
		override public bool HasAccessColumns()
		{
			return true;
		}
		PrimaryKeyField m_StationId = new PrimaryKeyField("StationId",null,true);
		override public PrimaryKeyField GetPrimaryKey()
		{
			return m_StationId;
		}
		public PrimaryKeyField StationId
		{
			get
			{
				return this.m_StationId;
			}
			set
			{
				this.m_StationId = value;
			}
		}
		IntField m_ModalityId = new IntField("ModalityId",null);
		public IntField ModalityId
		{
			get
			{
				return this.m_ModalityId;
			}
			set
			{
				this.m_ModalityId = value;
			}
		}
		TextField m_StationName = new TextField("StationName",null);
		public TextField StationName
		{
			get
			{
				return this.m_StationName;
			}
			set
			{
				this.m_StationName = value;
			}
		}
		TextField m_Instituition = new TextField("Instituition",null);
		public TextField Instituition
		{
			get
			{
				return this.m_Instituition;
			}
			set
			{
				this.m_Instituition = value;
			}
		}
		IntField m_ClientId = new IntField("ClientId",null);
		public IntField ClientId
		{
			get
			{
				return this.m_ClientId;
			}
			set
			{
				this.m_ClientId = value;
			}
		}
		IntField m_HospitalId = new IntField("HospitalId",null);
		public IntField HospitalId
		{
			get
			{
				return this.m_HospitalId;
			}
			set
			{
				this.m_HospitalId = value;
			}
		}
		override public Field[] GetFields()
		{
			Field[] fields = new Field[5];
			fields[0] = m_ModalityId;
			fields[1] = m_StationName;
			fields[2] = m_Instituition;
			fields[3] = m_ClientId;
			fields[4] = m_HospitalId;
			return fields;
		}
		override public Field[] GetAllFields()
		{
			Field[] fields = new Field[6];
			fields[0] = m_StationId;
			fields[1] = m_ModalityId;
			fields[2] = m_StationName;
			fields[3] = m_Instituition;
			fields[4] = m_ClientId;
			fields[5] = m_HospitalId;
			return fields;
		}
	}
}
