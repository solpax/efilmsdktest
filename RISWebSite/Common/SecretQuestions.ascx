﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecretQuestions.ascx.cs" Inherits="Common_SecretQuestions" %>
<asp:DropDownList ID="ddlSecretQuestions" runat="server">
    <asp:ListItem Text="What was your childhood nickname?" Value="1"></asp:ListItem>
    <asp:ListItem Text="In what city did you meet your spouse/significant other?" Value="2"></asp:ListItem>
    <asp:ListItem Text="What is the name of your favorite childhood friend?" Value="3"></asp:ListItem> 
    <asp:ListItem Text="What is your oldest sibling’s birthday month and year? (e.g., January 1900)" Value="4"></asp:ListItem>
    <asp:ListItem Text="What is the middle name of your youngest child?" Value="5"></asp:ListItem>
    <asp:ListItem Text="What is your oldest sibling's middle name?" Value="6"></asp:ListItem>
    <asp:ListItem Text="What was the name of your first pet?" Value="9"></asp:ListItem>
    <asp:ListItem Text="What was the last name of your favourite teacher?" Value="13"></asp:ListItem>
    <asp:ListItem Text="In what city or town was your first job?" Value="17"></asp:ListItem>    
    <asp:ListItem Text="What was the name of your first employer?" Value="22"></asp:ListItem>    
    <asp:ListItem Text="What was the name of your first car?" Value="23"></asp:ListItem>    
</asp:DropDownList>
